/*
   Garden monitor for soilmoisture with ESP01 and PCF9591
   Based on a sketch from Randomnerdtutorials https://randomnerdtutorials.com/esp32-esp8266-plot-chart-web-server/
   with various changes:
   using JSON for data
   plotting more lines in one graph
   data directly available, ratgher than after an interval

*/

#include "Wire.h"
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Hash.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <FS.h>

// Replace with your network credentials
const char* ssid = "TPLink25";//your ssid
const char* password = "secret1234";// your password

int PCF8591 = 0x48; // I2C bus address
byte ana0, ana1, ana2, ana3;
ADC_MODE(ADC_VCC);
float vdd = ESP.getVcc() / 1000.0;

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);


void setup()
{
  Wire.pins(0, 2); // just to make sure, this is for ESP01
  Wire.begin(0, 2); // the SDA and SCL
  Serial.begin(115200);
   // Initialize SPIFFS
  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  // Print ESP32 Local IP Address
  Serial.println(WiFi.localIP());

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html");
  });
  server.on("/moisture", HTTP_GET, [](AsyncWebServerRequest *request){
      String payload = "{\"Temp\":[" + String(ana0) + "," + String(ana1)+","+String(ana2)+","+String(ana3) + "]}";
    Serial.println(payload);
    request->send(200, "text/plain", payload.c_str());
  });
 
  server.on("/voltage", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(vdd).c_str());
  });

  // Start server
  server.begin();
}
void loop()
{
  Serial.println(vdd);
  adc();
  Serial.println(ana0);
  delay(2000);
}

void adc() {
  Wire.beginTransmission(PCF8591); // wake up PCF8591
  Wire.write(0x04); // control byte: reads ADC0 then auto-increment
  Wire.endTransmission(); // end tranmission
  Wire.requestFrom(PCF8591, 5);
  ana0 = Wire.read(); // throw this one away
  ana0 = Wire.read();
  ana1 = Wire.read();
  ana2 = Wire.read();
  ana3 = Wire.read();
  //uncomment the below lines for a quick test
  
  ana0=random(10,15);
  ana3=random(10);
  ana1=random(15,20);
  ana2=random(20,25);
  
}
void dac()
{
  Wire.beginTransmission(PCF8591); // wake up PCF8591
  Wire.write(0x40); // turn on DAC b1000000
  Wire.write(255);
  Wire.endTransmission();
}


/* 'circle', 'square','diamond', 'triangle' and 'triangle-down' */
